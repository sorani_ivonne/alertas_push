package com.example.alertas_push;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.alertas_push.Api.api;
import com.example.alertas_push.Api.service_peticion;
import com.example.alertas_push.ViewModels.Alerta;
import com.example.alertas_push.ViewModels.AlertasGlobales;
import com.example.alertas_push.ViewModels.AlertasUsuario;
import com.example.alertas_push.ViewModels.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaAlertasUsuario extends AppCompatActivity {

    List<Alerta> alertaList;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_alertas_usuario);

        alertaList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter(getApplicationContext(), alertaList);
        recyclerView.setAdapter(recyclerAdapter);

        service_peticion service = api.getApi(PantallaAlertasUsuario.this).create(service_peticion.class);
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String id = preferencias.getString("ID", "");
        if (id != "") {
            Call<AlertasUsuario> alertasUsuarioCall = service.getAlertasPorUsuario(id);
            alertasUsuarioCall.enqueue(new Callback<AlertasUsuario>() {
                @Override
                public void onResponse(Call<AlertasUsuario> call, Response<AlertasUsuario> response) {
                    AlertasUsuario alert = response.body();
                    if (alert.getEstado())
                        recyclerAdapter.setAlertasGlobalesList(alert.getAlertas());
                }

                @Override
                public void onFailure(Call<AlertasUsuario> call, Throwable t) {
                    Log.d("TAG", "Response = " + t.toString());
                    Toast.makeText(PantallaAlertasUsuario.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                    t.printStackTrace();
                }
            });
        }
    }
}
