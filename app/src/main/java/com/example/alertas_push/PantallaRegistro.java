package com.example.alertas_push;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alertas_push.Api.api;
import com.example.alertas_push.Api.service_peticion;
import com.example.alertas_push.ViewModels.Registro;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaRegistro extends AppCompatActivity {

    public Button registrar;
    public EditText user, pass1, pass2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registro);
        user = (EditText) findViewById(R.id.user);
        pass1 = (EditText) findViewById(R.id.pass1);
        pass2 = (EditText) findViewById(R.id.pass2);
        registrar = (Button) findViewById(R.id.registrar);
        String hola ="";
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!user.getText().toString().isEmpty() && !pass1.getText().toString().isEmpty() && !pass2.getText().toString().isEmpty()){
                    if (pass1.getText().toString().equals(pass2.getText().toString())){
                        service_peticion service = api.getApi(PantallaRegistro.this).create(service_peticion.class);
                        Call<Registro> registrarCall =  service.registrarUsuario(user.getText().toString(),pass2.getText().toString());
                        registrarCall.enqueue(new Callback<Registro>() {
                            @Override
                            public void onResponse(Call<Registro> call, Response<Registro> response) {
                                Registro peticion = response.body();
                                if(response.body() == null){
                                    Toast.makeText(PantallaRegistro.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                if(peticion.getEstado()){
                                    startActivity(new Intent(PantallaRegistro.this,MainActivity.class));
                                    Toast.makeText(PantallaRegistro.this, "Datos Registrados", Toast.LENGTH_LONG).show();
                                }else{
                                    Toast.makeText(PantallaRegistro.this, "Datos No Registrados", Toast.LENGTH_LONG).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<Registro> call, Throwable t) {
                                Toast.makeText(PantallaRegistro.this, "Error :(", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Toast.makeText(PantallaRegistro.this, "Contraseñas diferentes", Toast.LENGTH_SHORT).show();
                    }
                } else{
                    Toast.makeText(PantallaRegistro.this, "Campos vacíos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
