package com.example.alertas_push.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Visualizaciones {

    @SerializedName("visualizaciones")
    @Expose
    private List<Visualizacione> visualizaciones = null;

    public List<Visualizacione> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<Visualizacione> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }

    public Visualizaciones withVisualizaciones(List<Visualizacione> visualizaciones) {
        this.visualizaciones = visualizaciones;
        return this;
    }

}