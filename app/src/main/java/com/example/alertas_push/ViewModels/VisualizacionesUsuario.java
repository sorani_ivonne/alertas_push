package com.example.alertas_push.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VisualizacionesUsuario {

    @SerializedName("estado")
    @Expose
    private Boolean estado;
    @SerializedName("visualizaciones")
    @Expose
    private List<Visualizacione> visualizaciones = null;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public VisualizacionesUsuario withEstado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public List<Visualizacione> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<Visualizacione> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }

    public VisualizacionesUsuario withVisualizaciones(List<Visualizacione> visualizaciones) {
        this.visualizaciones = visualizaciones;
        return this;
    }

}
