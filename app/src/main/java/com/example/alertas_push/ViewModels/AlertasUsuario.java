package com.example.alertas_push.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlertasUsuario {

    @SerializedName("estado")
    @Expose
    private Boolean estado;
    @SerializedName("alertas")
    @Expose
    private List<Alerta> alertas = null;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public AlertasUsuario withEstado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public List<Alerta> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<Alerta> alertas) {
        this.alertas = alertas;
    }

    public AlertasUsuario withAlertas(List<Alerta> alertas) {
        this.alertas = alertas;
        return this;
    }

}
