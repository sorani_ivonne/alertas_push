package com.example.alertas_push.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Alerta {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("usuarioId")
    @Expose
    private String usuarioId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Alerta withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Alerta withUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Alerta withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Alerta withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

}
