package com.example.alertas_push.ViewModels;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.alertas_push.R;

import java.util.List;

public class RecyclerAdapterV extends RecyclerView.Adapter<RecyclerAdapterV.MyviewHolder> {

    Context context;
    List<Visualizacione> visualizacioneList;
    int globalposition;
    String parametro;

    public RecyclerAdapterV(Context context, List<Visualizacione> visualizacioneList) {
        this.context = context;
        this.visualizacioneList = visualizacioneList;
    }

    public void setVisualizacioneList(List<Visualizacione> visualizacioneList) {
        this.visualizacioneList = visualizacioneList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerAdapterV.MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_layout, parent, false);
        return new RecyclerAdapterV.MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapterV.MyviewHolder holder, int position) {
        holder.id.setText("id: " + visualizacioneList.get(position).getId()  + " Alert: " +visualizacioneList.get(position).getAlertaId());
        holder.usuarioId.setText("idUser: " + visualizacioneList.get(position).getUsuarioId());
        holder.createdAt.setText("created: " + visualizacioneList.get(position).getCreatedAt());
        holder.updatedAt.setText("updated: " + visualizacioneList.get(position).getUpdatedAt());
        holder.setOnClickListeners();
    }

    @Override
    public int getItemCount() {
        if (visualizacioneList != null) {
            return visualizacioneList.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView id, usuarioId, createdAt,updatedAt;
        public MyviewHolder(final View itemView) {
            super(itemView);
            context = itemView.getContext();
            id = (TextView) itemView.findViewById(R.id.id);
            usuarioId = (TextView) itemView.findViewById(R.id.usuarioId);
            createdAt = (TextView) itemView.findViewById(R.id.createdAt);
            updatedAt = (TextView) itemView.findViewById(R.id.updatedAt);
        }

        void setOnClickListeners(){
            usuarioId.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
        }
    }
}
