package com.example.alertas_push.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlertasGlobales {

    @SerializedName("alertas")
    @Expose
    private List<Alerta> alertas = null;

    public List<Alerta> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<Alerta> alertas) {
        this.alertas = alertas;
    }

    public AlertasGlobales withAlertas(List<Alerta> alertas) {
        this.alertas = alertas;
        return this;
    }

}
