package com.example.alertas_push.ViewModels;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.alertas_push.PantallaAlertasGlobales;
import com.example.alertas_push.PantallaRegistrarVisualizacion;
import com.example.alertas_push.R;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyviewHolder> {

    Context context;
    List<Alerta> alertasGlobalesList;
    int globalposition;
    String parametro;

    public RecyclerAdapter(Context context, List<Alerta> alertasGlobalesList) {
        this.context = context;
        this.alertasGlobalesList = alertasGlobalesList;
    }

    public void setAlertasGlobalesList(List<Alerta> alertasGlobalesList) {
        this.alertasGlobalesList = alertasGlobalesList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerAdapter.MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_layout, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.MyviewHolder holder, int position) {
        holder.id.setText("idAlerta: " + alertasGlobalesList.get(position).getId());
        holder.usuarioId.setText("idUser: " + alertasGlobalesList.get(position).getUsuarioId());
        holder.createdAt.setText("created: " + alertasGlobalesList.get(position).getCreatedAt());
        holder.updatedAt.setText("updated: " + alertasGlobalesList.get(position).getUpdatedAt());
        holder.setOnClickListeners();
    }

    @Override
    public int getItemCount() {
        if (alertasGlobalesList != null) {
            return alertasGlobalesList.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView id, usuarioId, createdAt,updatedAt;
        public MyviewHolder(final View itemView) {
            super(itemView);
            context = itemView.getContext();
            id = (TextView) itemView.findViewById(R.id.id);
            usuarioId = (TextView) itemView.findViewById(R.id.usuarioId);
            createdAt = (TextView) itemView.findViewById(R.id.createdAt);
            updatedAt = (TextView) itemView.findViewById(R.id.updatedAt);
        }

        void setOnClickListeners(){
            id.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, PantallaRegistrarVisualizacion.class);
            intent.putExtra("idAlerta", id.getText().toString().substring(10));
            intent.putExtra("idUser", usuarioId.getText().toString().substring(8));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}
