package com.example.alertas_push.Api;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.alertas_push.Menu;
import com.example.alertas_push.PantallaAlertasGlobales;
import com.example.alertas_push.R;
import com.example.alertas_push.ViewModels.AlertasGlobales;
import com.example.alertas_push.ViewModels.Registro;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fcm extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("token", "mi token es: "+s);
        GuardarToken(s);
    }

    public void GuardarToken(String s){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("token");
        ref.child("usuarioId").setValue(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String from = remoteMessage.getFrom();
        Log.e("TAG","Mensaje Recibido de: "+from);

        if(remoteMessage.getNotification() != null){
            Log.e("TAG","Titulo: "+remoteMessage.getNotification().getTitle());
            Log.e("TAG","Body: "+remoteMessage.getNotification().getBody());
        }
        // -- Clave valor
        if(remoteMessage.getData().size() > 0){

            Log.e("TAG","Mi titulo es "+remoteMessage.getData().get("titulo"));
            Log.e("TAG","Mi detalle es "+remoteMessage.getData().get("detalle"));
            Log.e("TAG","Mi color es "+remoteMessage.getData().get("aColor"));

            String titulo = remoteMessage.getData().get("titulo");
            String detalle = remoteMessage.getData().get("detalle");
            mayorQueOreo(titulo,detalle);
        }
    }
    private void mayorQueOreo(String titulo, String detalle) {
        String id = "mensaje";
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, id);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel nc = new NotificationChannel(id,"nuevo",NotificationManager.IMPORTANCE_HIGH);
            nc.setShowBadge(true);
            assert nm != null;
            nm.createNotificationChannel(nc);
        }
        builder.setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(titulo)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(detalle)
                .setContentIntent(clicknoti())
                .setContentInfo("nuevo");
        Random random = new Random();
        int idnoti = random.nextInt(8000);
        assert nm != null;
        nm.notify(idnoti, builder.build());
    }

    private PendingIntent clicknoti() {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String id = preferencias.getString("ID", "");
        if(id != ""){
            service_peticion service = api.getApi(Fcm.this).create(service_peticion.class);
            Call<Registro> registroCall = service.registrarAlerta(id);
            registroCall.enqueue(new Callback<Registro>() {
                @Override
                public void onResponse(Call<Registro> call, Response<Registro> response) {
                    Registro reg = response.body();
                    if (reg.getEstado()){
                        Toast.makeText(Fcm.this, "Alerta Creada", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Registro> call, Throwable t) {

                }
            });
        }
        Intent it = new Intent(getApplicationContext(), Menu.class);
        it.putExtra("color","rojo");
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(this,0,it,0);
    }
}
