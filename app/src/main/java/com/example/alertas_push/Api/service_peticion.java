package com.example.alertas_push.Api;

import com.example.alertas_push.ViewModels.AlertasGlobales;
import com.example.alertas_push.ViewModels.AlertasUsuario;
import com.example.alertas_push.ViewModels.Login;
import com.example.alertas_push.ViewModels.Registro;
import com.example.alertas_push.ViewModels.Visualizaciones;
import com.example.alertas_push.ViewModels.VisualizacionesUsuario;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface service_peticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/crearAlerta")
    Call<Registro> registrarAlerta(@Field("usuarioId") String id);

    @FormUrlEncoded
    @POST("api/alertasusuario")
    Call<AlertasUsuario> getAlertasPorUsuario(@Field("usuarioId") String id);

    @POST("api/alertas")
    Call<AlertasGlobales> getTodasAlertas();

    @FormUrlEncoded
    @POST("api/crearvisualizacionalerta")
    Call<Registro> registrarVisualizacion(@Field("usuarioId") String id, @Field("alertaId") String ida);

    @FormUrlEncoded
    @POST("api/visualizacionesusuario")
    Call<VisualizacionesUsuario> getVisualizacionPorUsuario(@Field("usuarioId") String id);

    @POST("api/visualizaciones")
    Call<Visualizaciones> getTodasVisualizaciones();
}
