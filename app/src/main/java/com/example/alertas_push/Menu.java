package com.example.alertas_push;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Menu extends AppCompatActivity {

    Button user, notas, nota, hola, cerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        notas = (Button) findViewById(R.id.button2);
        user = (Button) findViewById(R.id.button3);
        nota = (Button) findViewById(R.id.nota);
        hola = (Button) findViewById(R.id.button);
        cerrar = (Button) findViewById(R.id.button4);
        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, PantallaAlertasGlobales.class);
                startActivity(intent);
            }
        });
        notas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, PantallaAlertasUsuario.class);
                startActivity(intent);
            }
        });
        nota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, PantallaVisualizacionesGlobales.class);
                startActivity(intent);
            }
        });
        hola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, PantallaVisualizacionesUsuario.class);
                startActivity(intent);
            }
        });
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferencias.edit();
                String tokenn = preferencias.getString("TOKENN", "");
                if(tokenn != ""){
                    editor.remove("TOKENN");
                    editor.commit();
                    Toast.makeText(Menu.this, "Cerraste sesion", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Menu.this, MainActivity.class));
                }
            }
        });
    }
}
