package com.example.alertas_push;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.alertas_push.Api.api;
import com.example.alertas_push.Api.service_peticion;
import com.example.alertas_push.ViewModels.Alerta;
import com.example.alertas_push.ViewModels.AlertasUsuario;
import com.example.alertas_push.ViewModels.RecyclerAdapter;
import com.example.alertas_push.ViewModels.RecyclerAdapterV;
import com.example.alertas_push.ViewModels.Visualizacione;
import com.example.alertas_push.ViewModels.VisualizacionesUsuario;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaVisualizacionesUsuario extends AppCompatActivity {

    List<Visualizacione> visualizacioneUsuarioList;
    RecyclerView recyclerView;
    RecyclerAdapterV recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_alertas_usuario);

        visualizacioneUsuarioList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapterV(getApplicationContext(), visualizacioneUsuarioList);
        recyclerView.setAdapter(recyclerAdapter);

        service_peticion service = api.getApi(PantallaVisualizacionesUsuario.this).create(service_peticion.class);
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String id = preferencias.getString("ID", "");
        if (id != "") {
            Call<VisualizacionesUsuario> visualizacionesUsuarioCall = service.getVisualizacionPorUsuario(id);
            visualizacionesUsuarioCall.enqueue(new Callback<VisualizacionesUsuario>() {
                @Override
                public void onResponse(Call<VisualizacionesUsuario> call, Response<VisualizacionesUsuario> response) {
                    VisualizacionesUsuario visualizacionesUsuario = response.body();
                    if (visualizacionesUsuario.getEstado())
                        recyclerAdapter.setVisualizacioneList(visualizacionesUsuario.getVisualizaciones());
                }

                @Override
                public void onFailure(Call<VisualizacionesUsuario> call, Throwable t) {
                    Log.d("TAG", "Response = " + t.toString());
                    Toast.makeText(PantallaVisualizacionesUsuario.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                    t.printStackTrace();
                }
            });
        }
    }
}
