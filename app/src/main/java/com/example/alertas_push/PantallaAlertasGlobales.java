package com.example.alertas_push;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.alertas_push.Api.api;
import com.example.alertas_push.Api.service_peticion;
import com.example.alertas_push.ViewModels.Alerta;
import com.example.alertas_push.ViewModels.AlertasGlobales;
import com.example.alertas_push.ViewModels.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaAlertasGlobales extends AppCompatActivity {

    List<Alerta> alertasGlobalesList;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_alertas_globales);

        alertasGlobalesList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter(getApplicationContext(), alertasGlobalesList);
        recyclerView.setAdapter(recyclerAdapter);

        service_peticion service = api.getApi(PantallaAlertasGlobales.this).create(service_peticion.class);
        Call<AlertasGlobales> alertasGlobalesCall = service.getTodasAlertas();
        alertasGlobalesCall.enqueue(new Callback<AlertasGlobales>() {
            @Override
            public void onResponse(Call<AlertasGlobales> call, Response<AlertasGlobales> response) {
                AlertasGlobales alert = response.body();
                recyclerAdapter.setAlertasGlobalesList(alert.getAlertas());
            }

            @Override
            public void onFailure(Call<AlertasGlobales> call, Throwable t) {

            }
        });

    }
}
