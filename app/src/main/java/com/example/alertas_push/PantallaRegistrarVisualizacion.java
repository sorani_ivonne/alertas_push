package com.example.alertas_push;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alertas_push.Api.api;
import com.example.alertas_push.Api.service_peticion;
import com.example.alertas_push.ViewModels.Registro;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaRegistrarVisualizacion extends AppCompatActivity {

    public Button registrar;
    public EditText user, pass1;
    public String idAlerta, idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registrar_visualizacion);
        user = (EditText) findViewById(R.id.user);
        pass1 = (EditText) findViewById(R.id.pass1);
        registrar = (Button) findViewById(R.id.registrar);
        idAlerta = getIntent().getExtras().getString("idAlerta");
        idUser = getIntent().getExtras().getString("idUser");
        user.setText(idAlerta);
        pass1.setText(idUser);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!user.getText().toString().isEmpty() && !pass1.getText().toString().isEmpty()) {
                    service_peticion service = api.getApi(PantallaRegistrarVisualizacion.this).create(service_peticion.class);
                    Call<Registro> registrarCall = service.registrarVisualizacion(pass1.getText().toString(), user.getText().toString());
                    registrarCall.enqueue(new Callback<Registro>() {
                        @Override
                        public void onResponse(Call<Registro> call, Response<Registro> response) {
                            Registro peticion = response.body();
                            if (response.body() == null) {
                                Toast.makeText(PantallaRegistrarVisualizacion.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (peticion.getEstado()) {
                                startActivity(new Intent(PantallaRegistrarVisualizacion.this, Menu.class));
                                Toast.makeText(PantallaRegistrarVisualizacion.this, "Visualizacion hecha", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(PantallaRegistrarVisualizacion.this, "Visualizacion no hecha", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Registro> call, Throwable t) {
                            Toast.makeText(PantallaRegistrarVisualizacion.this, "Error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(PantallaRegistrarVisualizacion.this, "Campos vacíos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
