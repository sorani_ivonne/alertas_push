package com.example.alertas_push;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.alertas_push.Api.api;
import com.example.alertas_push.Api.service_peticion;
import com.example.alertas_push.ViewModels.Alerta;
import com.example.alertas_push.ViewModels.AlertasGlobales;
import com.example.alertas_push.ViewModels.RecyclerAdapter;
import com.example.alertas_push.ViewModels.RecyclerAdapterV;
import com.example.alertas_push.ViewModels.Visualizacione;
import com.example.alertas_push.ViewModels.Visualizaciones;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaVisualizacionesGlobales extends AppCompatActivity {

    List<Visualizacione> visualizacionesGlobales;
    RecyclerView recyclerView;
    RecyclerAdapterV recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_visualizaciones_globales);

        visualizacionesGlobales = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapterV(getApplicationContext(), visualizacionesGlobales);
        recyclerView.setAdapter(recyclerAdapter);

        service_peticion service = api.getApi(PantallaVisualizacionesGlobales.this).create(service_peticion.class);
        Call<Visualizaciones> todasVisualizaciones = service.getTodasVisualizaciones();
        todasVisualizaciones.enqueue(new Callback<Visualizaciones>() {
            @Override
            public void onResponse(Call<Visualizaciones> call, Response<Visualizaciones> response) {
                Visualizaciones visualizacion = response.body();
                recyclerAdapter.setVisualizacioneList(visualizacion.getVisualizaciones());
            }

            @Override
            public void onFailure(Call<Visualizaciones> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
                Toast.makeText(PantallaVisualizacionesGlobales.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });

    }
}
