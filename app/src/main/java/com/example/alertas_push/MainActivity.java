package com.example.alertas_push;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alertas_push.Api.api;
import com.example.alertas_push.Api.service_peticion;
import com.example.alertas_push.ViewModels.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText correo, contrasena;
    TextView textView5;
    Button login;
    public String APITOKEN = "";
    public String id = "";;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String tokenn = preferencias.getString("TOKENN", "");
        if(tokenn != ""){
            Toast.makeText(MainActivity.this, "Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(MainActivity.this, Menu.class));
        }

        correo = (EditText) findViewById(R.id.correo);
        contrasena = (EditText) findViewById(R.id.contrasena);
        login = (Button) findViewById(R.id.login);
        textView5 = (TextView) findViewById(R.id.textView5);

        textView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PantallaRegistro.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!correo.getText().toString().isEmpty() && !contrasena.getText().toString().isEmpty()){
                    service_peticion service = api.getApi(MainActivity.this).create(service_peticion.class);
                    Call<Login> loginCall =  service.getLogin(correo.getText().toString(),contrasena.getText().toString());
                    loginCall.enqueue(new Callback<Login>() {
                        @Override
                        public void onResponse(Call<Login> call, Response<Login> response) {
                            Login peticion = response.body();
                            if(peticion.getEstado()){
                                APITOKEN = peticion.getToken();
                                id = peticion.getId().toString();
                                guardarPreferencias();
                                Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(MainActivity.this, Menu.class));
                            }else{
                                Toast.makeText(MainActivity.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Login> call, Throwable t) {
                            Toast.makeText(MainActivity.this, "Error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else{
                    Toast.makeText(MainActivity.this, "Llene campos please", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void guardarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String tokenn = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKENN", tokenn);
        editor.putString("ID", id);
        editor.commit();
    }
}
